**_Cron Expression Parser_**

This command line application parses a cron string and expands each field to show the times at which the command will run.

**Building the application**
This is a maven project. The project is built using `mvn clean install` command.
A jar named `deliveroo-cron-parser-1.0-SNAPSHOT.jar` should be created under _**target/**_ folder.

**Running the application**
Command used to run the application `$ java -jar target/deliveroo-cron-parser-1.0-SNAPSHOT.jar "<input_string>"`

`input_string` should be passed in this format : [minute] [hour] [day of month] [month] [day of week] [command]

###### `Example: */15 0 1,15 * 1-5 /usr/bin/find`

Sample command to run the application

###### `java -jar target/cron-parser-deliveroo-1.0.jar "*/15 0 1,15 * 1-5 /usr/bin/find"`

Sample output of the application

```
minute 			0 15 30 45
hour   			0
day of month 	1 15
month			1 2 3 4 5 6 7 8 9 10 11 12
day of week 	1 2 3 4 5
command			/usr/bin/find
```

**Approach** <br />
Cron format supports multiple input types : <br />

- 5 : a fixed value [FixedTimingParser](https://bitbucket.org/agarwalrajat724/cron-parser/src/master/src/main/java/com/deliveroo/cron/parsers/FixedTimingParser.java)<br />
- 5-10 : a range [RangeParser](https://bitbucket.org/agarwalrajat724/cron-parser/src/master/src/main/java/com/deliveroo/cron/parsers/RangeParser.java)<br />
- */5 : a frequency [IntervalParser](https://bitbucket.org/agarwalrajat724/cron-parser/src/master/src/main/java/com/deliveroo/cron/parsers/IntervalParser.java)<br />
- 5,7 : a list of values [FixedTimingParser](https://bitbucket.org/agarwalrajat724/cron-parser/src/master/src/main/java/com/deliveroo/cron/parsers/FixedTimingParser.java)<br />

Each time component (minutes, hours, etc..) has a specific range defined in [CronUnit](https://bitbucket.org/agarwalrajat724/cron-parser/src/master/src/main/java/com/deliveroo/cron/model/CronUnit.java)
