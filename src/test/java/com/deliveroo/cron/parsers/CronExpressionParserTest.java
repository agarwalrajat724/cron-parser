package com.deliveroo.cron.parsers;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.parsers.strategy.UnixTypeParserStrategy;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class CronExpressionParserTest {
    private static CronExpressionParser parser;

    @BeforeAll
    public static void init() {
        var parserStrategy = new UnixTypeParserStrategy();
        parserStrategy.registerStrategy(new AnyParser());
        parserStrategy.registerStrategy(new IntervalParser());
        parserStrategy.registerStrategy(new FixedTimingParser());
        parserStrategy.registerStrategy(new RangeParser());
        parser = new CronExpressionParser(parserStrategy);
    }

    @Test
    public void testIfCronExpressionIsWorkingAsExpected() {
        init();
        var expected =
                "minute        0 15 30 45\n" +
                "hour          0\n" +
                "day of month  1 15\n" +
                "month         1 2 3 4 5 6 7 8 9 10 11 12\n" +
                "day of week   1 2 3 4 5\n";

        var cronExpression = "*/15 0 1,15 * 1-5";

        var cron = parser.parseCronString(cronExpression);
        Assertions.assertEquals(expected, cron.asString());
    }

    private static Stream<Arguments> exceptionDataForParser() {
        return Stream.of(
                Arguments.of("**/15 0 1,15 * 1-5"),
                Arguments.of("*/67 0 1,15 * 1-5"),
                Arguments.of("* 0 1,15 * 10")
        );
    }

    @ParameterizedTest
    @MethodSource("exceptionDataForParser")
    public void testIfCronExpressionParserThrowsExceptionForInvalidInput(String cronExpression) {
        assertThrows(InvalidCronExpression.class, () -> {
            parser.parseCronString(cronExpression);
        });
    }
}
