package com.deliveroo.cron.service;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.parsers.strategy.ParserStrategyType;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CronParsingServiceTest {
    private final CronParsingService parsingService = new CronParsingService(ParserStrategyType.DEFAULT);

    @Test
    public void testIfCronExpressionIsWorkingAsExpected() {
        var expected =
                        "minute        0 15 30 45\n" +
                        "hour          0\n" +
                        "day of month  1 15\n" +
                        "month         1 2 3 4 5 6 7 8 9 10 11 12\n" +
                        "day of week   1 2 3 4 5\n" +
                        "command       /usr/bin/find\n";

        var cronExpression = "*/15 0 1,15 * 1-5 /usr/bin/find";

        var cronParsedResponse = parsingService.parseString(cronExpression);
        assertEquals(expected, cronParsedResponse.toString());
    }

    private static Stream<Arguments> exceptionDataForParser() {
        return Stream.of(
                Arguments.of(""),
                Arguments.of("**/15 0 1,15 * 1-5"),
                Arguments.of("*/67 0 1,15 * 1-5"),
                Arguments.of("* 0 1,15 * 10 /usr/bin/find /usr/lib/find")

        );
    }

    @ParameterizedTest
    @MethodSource("exceptionDataForParser")
    public void testIfCronExpressionParserThrowsExceptionForInvalidInput(String cronExpression) {
        Assertions.assertThrows(InvalidCronExpression.class, () -> {
            parsingService.parseString(cronExpression);
        });
    }
}