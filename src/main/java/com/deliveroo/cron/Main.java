package com.deliveroo.cron;


import org.apache.commons.lang3.StringUtils;

import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.parsers.strategy.ParserStrategyType;
import com.deliveroo.cron.service.CronParsingService;

public class Main {

    public static void main(String[] args) {
        new Main().parseString(args);
    }

    private void parseString(String[] arguments) {
        if (arguments.length != 1 || arguments[0].split(StringUtils.SPACE).length != 6) {
            System.out.println("Input : " + arguments[0]);
            printUsage();
            System.err.println("ERROR: Arguments passed to the program: " + String.join(" ", arguments));
            return;
        }
        try {
            var cronParsingService = new CronParsingService(ParserStrategyType.DEFAULT);
            var response = cronParsingService.parseString(arguments[0]);
            System.out.println(response);
        } catch (InvalidCronExpression ex) {
            System.err.println(ex.getMessage());
            System.out.println("\n");
            printUsage();
        }
    }

    private static void printUsage() {
        System.out.println("USAGE: ");
        System.out.println("Pass the cron expression arguments in the same order");
        System.out.println("[minute] [hour] [day of month] [month] [day of week] [command]");
        System.out.println("Example: */15 0 1,15 * 1-5 /usr/bin/find");
        System.out.println();
    }
}
