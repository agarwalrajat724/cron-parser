package com.deliveroo.cron.service;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;

import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.model.CronParsedResponse;
import com.deliveroo.cron.parsers.CronExpressionParser;
import com.deliveroo.cron.parsers.strategy.ParserStrategyFactory;
import com.deliveroo.cron.parsers.strategy.ParserStrategyType;

/**
 * Extracts and parses Cron string and Command.
 */
public class CronParsingService {

    private final CronExpressionParser cronExpressionParser;

    public CronParsingService(final ParserStrategyType parserStrategyType) {
        var parserStrategy = ParserStrategyFactory.createParserStrategy(parserStrategyType);
        this.cronExpressionParser = new CronExpressionParser(parserStrategy);
    }

    public CronParsedResponse parseString(final String expression) {
        if (StringUtils.isBlank(expression)) {
            throw new InvalidCronExpression("Expression Can't be NULL or Empty");
        }
        var expressions = expression.split(StringUtils.SPACE);
        if (expressions.length != 6) {
            throw new InvalidCronExpression("ERROR: Wrong No of Arguments passed to the program: " + String.join(" ", expressions));
        }
        var cronExpression = getCronExpression(expressions);

        var cron = cronExpressionParser.parseCronString(cronExpression);
        var command = expressions[expressions.length - 1];

        return new CronParsedResponse(cron, command);
    }

    private String getCronExpression(String[] expressions) {
        return IntStream.range(0, expressions.length - 1)
                .boxed()
                .map(index -> expressions[index])
                .collect(Collectors.joining(StringUtils.SPACE));
    }
}
