package com.deliveroo.cron.exception;

import com.deliveroo.cron.model.CronUnit;

/**
 *  Custom Exception Class for IllegalArgumentException
 */
public class InvalidCronExpression extends RuntimeException {

    public InvalidCronExpression(final String message) {
        super(message);
    }

    public InvalidCronExpression(final CronUnit cronUnit, final String cronExpression, final String message) {
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("The expresssion " + cronExpression + " passed for time unit " + cronUnit.getName() + " is invalid. \n");
        errorMessage.append("Accepted range for time unit " + cronUnit.getName() + " is [" + cronUnit.getStartRange() +"-" + cronUnit.getEndRange() +"]. \n");
        errorMessage.append("Error message is : " + message);
        throw new InvalidCronExpression(errorMessage.toString());
    }
}
