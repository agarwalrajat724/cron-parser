package com.deliveroo.cron.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import static java.lang.String.format;

/**
 * For Parsed Response of the Expression.
 */
@Data
@AllArgsConstructor
public class CronParsedResponse {
    private Cron cron;
    private String command;

    @Override
    public String toString() {
        StringBuilder response = new StringBuilder();
        response.append(cron.asString());
        response.append(format("%-14s%s\n", "command", command));
        return response.toString();
    }
}
