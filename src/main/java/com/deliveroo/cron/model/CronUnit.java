package com.deliveroo.cron.model;

/**
 * Enumerates cron field names of the Expression.
 */
public enum CronUnit {

    MINUTE(0, "minute", 0, 59),
    HOUR(1, "hour", 0, 23),
    DAY_OF_MONTH(2, "day of month", 1, 31),
    MONTH(3, "month", 1, 12),
    DAY_OF_WEEK(4, "day of week", 1, 7);

    private final int order;
    private final String name;
    private final int startRange;
    private final int endRange;

    CronUnit(final int order, final String name, final int startRange, final int endRange) {
        this.order = order;
        this.name = name;
        this.startRange = startRange;
        this.endRange = endRange;
    }

    public int getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    public int getStartRange() {
        return startRange;
    }

    public int getEndRange() {
        return endRange;
    }
}
