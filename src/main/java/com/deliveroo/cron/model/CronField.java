package com.deliveroo.cron.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * For Mapping CronUnit to the List of Executable Time Units.
 */
@Data
@AllArgsConstructor
public class CronField {
    private final CronUnit cronUnit;
    private final List<Integer> timings;
}
