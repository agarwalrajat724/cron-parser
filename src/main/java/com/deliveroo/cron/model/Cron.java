package com.deliveroo.cron.model;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;
import static java.lang.String.format;

/**
 * POJO for all the CronFields
 */
@Data
public class Cron {

    private final List<CronField> cronFields;

    private String asString;

    public String asString() {
        if (StringUtils.isNotBlank(asString)) {
            return asString;
        }
        StringBuilder parsedCronBuilder = new StringBuilder();

        cronFields.forEach(cronField -> parsedCronBuilder.append(format("%-14s%s\n", cronField.getCronUnit().getName(), printList(cronField.getTimings()))));
        asString = parsedCronBuilder.toString();
        return asString;
    }

    private String printList(List<Integer> integers) {
        return integers.stream().map(Object::toString).collect(Collectors.joining(" "));
    }
}
