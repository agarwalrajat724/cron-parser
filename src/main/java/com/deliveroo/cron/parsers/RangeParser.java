package com.deliveroo.cron.parsers;

import java.util.ArrayList;
import java.util.List;

import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.model.CronUnit;

/**
 * For Bounded Ranges. Eg:- 1-5
 */
public class RangeParser extends Parser {

    private static final String DASH_DELIMITER = "-";

    private static final String BOUNDED_INTERVAL_REGEX = "^\\d+-\\d+(,\\d+-\\d+)*$";
    
    @Override
    public List<Integer> getTimings(CronUnit cronUnit, String cronExpression) throws InvalidCronExpression {
        String[] intervals = cronExpression.split(DASH_DELIMITER);
        Integer startInterval = Integer.valueOf(intervals[0]);
        Integer endInterval = Integer.valueOf(intervals[1]);

        if (!isValid(startInterval, endInterval, cronUnit)) {
            throw new InvalidCronExpression(cronUnit, cronExpression, "Values passed are not in given range");
        }
        return new ArrayList<>(getCronTimings(startInterval, endInterval, 1));
    }

    @Override
    public String getRegex() {
        return BOUNDED_INTERVAL_REGEX;
    }

    private boolean isValid(Integer startInterval, Integer endInterval, CronUnit cronUnit) {
        return isInRange(cronUnit.getStartRange(), cronUnit.getEndRange(), startInterval) &&
                isInRange(cronUnit.getStartRange(), cronUnit.getEndRange(), endInterval) &&
                startInterval <= endInterval;
    }
}
