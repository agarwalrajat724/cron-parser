package com.deliveroo.cron.parsers;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.model.Cron;
import com.deliveroo.cron.model.CronUnit;
import com.deliveroo.cron.model.CronField;
import com.deliveroo.cron.parsers.strategy.ParserStrategy;

/**
 * Parses the String representation of the Cron Time Units to CronFields with executable Timing Units.
 */
public class CronExpressionParser {

    private final ParserStrategy parserStrategy;

    public CronExpressionParser(final ParserStrategy parserStrategy) {
        this.parserStrategy = parserStrategy;
    }

    public Cron parseCronString(final String cronExpression) {
        if (StringUtils.isBlank(cronExpression)) {
            throw new InvalidCronExpression("Cron Expression can't be NULL or Empty");
        }

        var cronExpressions = cronExpression.split(StringUtils.SPACE);

        var cronFields = Arrays.stream(CronUnit.values())
                .map(toCronField(cronExpressions))
                .collect(Collectors.toList());

        return new Cron(cronFields);
    }

    private Function<CronUnit, CronField> toCronField(String[] cronExpressions) {
        return cronUnit -> {
            var expression = cronExpressions[cronUnit.getOrder()];
            var timings = parserStrategy.parse(cronUnit, expression);
            return new CronField(cronUnit, timings);
        };
    }
}
