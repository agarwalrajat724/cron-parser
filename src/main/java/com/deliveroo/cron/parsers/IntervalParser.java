package com.deliveroo.cron.parsers;

import java.util.List;

import com.deliveroo.cron.model.CronUnit;
import com.deliveroo.cron.exception.InvalidCronExpression;

/**
 * For Frequency Delimeter (*\/) parsing. Eg:- *\/15 for mins -> 0,15,30,45
 */
public class IntervalParser extends Parser {

    private static final String NTH_INTERVAL_FORMAT = "*/";

    private static final String ASTERISK_SLASH_REGEX = "^\\*/\\d+$";

    @Override
    public List<Integer> getTimings(CronUnit timeUnit, String cronExpression) throws InvalidCronExpression {
        String interval = cronExpression.substring(NTH_INTERVAL_FORMAT.length());

        int value = Integer.parseInt(interval);

        if (!isInRange(timeUnit.getStartRange(), timeUnit.getEndRange(), value)) {
            throw new InvalidCronExpression(timeUnit, cronExpression, "Values passed are not in given range");
        }

        return getCronTimings(timeUnit, value);
    }

    @Override
    public String getRegex() {
        return ASTERISK_SLASH_REGEX;
    }
}
