package com.deliveroo.cron.parsers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.deliveroo.cron.model.CronUnit;
import com.deliveroo.cron.exception.InvalidCronExpression;

public abstract class Parser {

    protected List<Integer> getCronTimings(final CronUnit cronUnit, final Integer increment) {
        return getCronTimings(cronUnit.getStartRange(), cronUnit.getEndRange(), increment);
    }

    protected List<Integer> getCronTimings(final Integer startRange, final Integer endRange, final Integer increment) {
        return IntStream
                .iterate(startRange, nextRange -> nextRange <= endRange, nextRange -> nextRange + increment)
                .boxed()
                .collect(Collectors.toList());
    }

    protected boolean isInRange(final Integer startRange, final Integer endRange, final Integer value) {
        return value >= startRange && value <= endRange;
    }

    public abstract List<Integer> getTimings(final CronUnit cronUnit, final String cronExpression) throws InvalidCronExpression;

    public abstract String getRegex();
}
