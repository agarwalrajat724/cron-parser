package com.deliveroo.cron.parsers;

import java.util.List;

import com.deliveroo.cron.model.CronUnit;
import com.deliveroo.cron.exception.InvalidCronExpression;

/**
 * For Single Timing Units or the integer values withing the range.
 */
public class FixedTimingParser extends Parser {

    private static final String DIGIT_REGEX = "^\\d+$";

    @Override
    public List<Integer> getTimings(CronUnit cronUnit, String cronExpression) throws InvalidCronExpression {
        Integer value = Integer.valueOf(cronExpression);
        if (!isInRange(cronUnit.getStartRange(), cronUnit.getEndRange(), value)) {
            throw new InvalidCronExpression(cronUnit, cronExpression, "Values passed are not in given range");
        }
        return List.of(value);
    }

    @Override
    public String getRegex() {
        return DIGIT_REGEX;
    }
}
