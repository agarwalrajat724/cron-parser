package com.deliveroo.cron.parsers.strategy;

import java.util.List;

import com.deliveroo.cron.model.CronUnit;
import com.deliveroo.cron.parsers.Parser;

public interface ParserStrategy {

    void registerStrategy(Parser parser);

    boolean removeStrategy(Parser parser);

    List<Integer> parse(CronUnit cronUnit, String cronExpression);
}
