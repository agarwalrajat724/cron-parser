package com.deliveroo.cron.parsers.strategy;

import java.util.List;

import com.deliveroo.cron.parsers.AnyParser;
import com.deliveroo.cron.parsers.FixedTimingParser;
import com.deliveroo.cron.parsers.IntervalParser;
import com.deliveroo.cron.parsers.RangeParser;

public class ParserStrategyFactory {

    public static ParserStrategy createParserStrategy(final ParserStrategyType parserStrategyType) {
        if (null == parserStrategyType) {
            return createUnixTypeParser();
        }
        switch (parserStrategyType) {
            case DEFAULT:
            case UNIX:
                return createUnixTypeParser();
            default: throw new IllegalArgumentException("Unknown Parser Strategy : " + parserStrategyType);
        }
    }

    private static ParserStrategy createUnixTypeParser() {
        var parserStrategyManager = new UnixTypeParserStrategy();

        var parsers = List.of(
                new AnyParser(),
                new FixedTimingParser(),
                new IntervalParser(),
                new RangeParser()
        );

        parsers.forEach(parserStrategyManager::registerStrategy);

        return parserStrategyManager;
    }
}
