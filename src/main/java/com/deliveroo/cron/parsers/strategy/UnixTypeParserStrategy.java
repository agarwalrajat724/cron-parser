package com.deliveroo.cron.parsers.strategy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.deliveroo.cron.model.CronUnit;
import com.deliveroo.cron.exception.InvalidCronExpression;
import com.deliveroo.cron.parsers.Parser;

/**
 * For UNIX Type Parsing Strategy.
 */
public class UnixTypeParserStrategy implements ParserStrategy {

    private final Set<Parser> parsers = new HashSet<>();

    private static final String COMMA_DELIMITER = ",";

    @Override
    public void registerStrategy(Parser parser) {
        parsers.add(parser);
    }

    @Override
    public boolean removeStrategy(Parser parser) {
        return parsers.remove(parser);
    }

    @Override
    public List<Integer> parse(CronUnit cronUnit, String cronExpression) {
        var timings = new TreeSet<Integer>();
        var cronExpressions = cronExpression.split(COMMA_DELIMITER);

        for (String expr: cronExpressions) {
            var parser = findParser(cronUnit, expr);
            timings.addAll(parser.getTimings(cronUnit, expr));
        }

        return new ArrayList<>(timings);
    }

    private Parser findParser(CronUnit cronUnit, String cronExpression) {
        var optionalParser = parsers
                .stream()
                .filter(parser -> cronExpression.matches(parser.getRegex()))
                .findFirst();

        return optionalParser
                .orElseThrow(
                        () -> new InvalidCronExpression(cronUnit, cronExpression, "Invalid expression passed. Not able to parse the given input.")
                );
    }
}
