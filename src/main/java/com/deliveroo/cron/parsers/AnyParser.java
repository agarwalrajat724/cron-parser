package com.deliveroo.cron.parsers;

import java.util.List;

import com.deliveroo.cron.model.CronUnit;

/**
 * For Parsing the Cron Expr - *, which maps to all the units within the Range.
 */
public class AnyParser extends Parser {

    private static final String ASTERISK_REGEX = "^\\*$";

    @Override
    public List<Integer> getTimings(CronUnit timeUnit, String cronExpression) {
        return getCronTimings(timeUnit, 1);
    }

    @Override
    public String getRegex() {
        return ASTERISK_REGEX;
    }
}
